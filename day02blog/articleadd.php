<?php require_once 'db.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
  function displayForm($title = "", $body = ""){
      // here doc example
      $formLLL = <<< END
      <form method="post">
        Name: <input name="title" type="text" value="$title"><br>
        <textarea name="body" cols="60" row="10">$body</textarea><br>
        <input type="submit" value="Post Article">
    </form>
    END;
    echo $formLLL;
}

if (isset($_POST['title'])) { // we're receving a submission
    $name = $_POST['title'];
    $body = $_POST['body'];
    // verify inputs
    $errorList = array();
    if (strlen($title) < 2 || strlen($title) > 100) {
        $errorList[] = "Title must be 2-100 characters long";
        $title = "";
    }
    if (strlen($body) < 2 || strlen($body) > 4000) {
        $errorList[] = "Body must be 2-4000 characters long";
        $body = "";
    }
    //
    if ($errorList) { // STATE 2: submission with errors (failed)
        echo '<ul class="errorMessage">';
        foreach ($errorList as $error) {
            echo "<li>$error</li>\n";
        }
        echo '</ul>';
        displayForm($title, $body);
    } else { // STATE 3: submission successful
        echo "Hello $name, you are $age y/o.";
    }
} else { // STATE 1: first show
    displayForm();
}
  
?>
</body>
</html>