<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        // *************** Numbers ***************

        // basic operators - *, /, +, -, **
        // Order of operation (Brackets, Indeces, Division, Multiplication, Addition, Substraction)

        $radius = 25;
        $pi = 3.14;

        echo $pi *$radius**2; 
        echo "<br>"; // next line

        // incrementation

        $number = 20;
        $number += 10;
        echo $number;
        echo "<br>"; // next line

        floor($pi); // 3.14 -> 3
        ceil($pi); // 3.14 -> 4

        pi(); // gives actual Pi value
    ?>
</body>
</html>