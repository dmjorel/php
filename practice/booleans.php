<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        // *************** Boolean ***************

        //echo true;  "1"
        //echo false;  ""

        echo 5 < 10; // true, shows "1" 
        echo 5 > 10; // false, shows ""
        echo 5 == 10; // false
        echo 5 != 10; // true
        echo 5 <= 5; // true
        echo 5 >= 5; // true

        echo 'shaun' < 'yoshi'; // true, alphabet s before/smaller than y
        echo 'shaun' > 'yoshi'; // false
        echo 'shaun' > 'Shaun'; // true, lower is bigger than capital
        echo 'mario' == 'mario'; // true
        echo 'mario' == 'Mario'; // false

        // loose vs strict equal comparison
        echo 5 == '5'; // true, regardless of type
        echo 5 === '5'; // false, different data type  
        
        echo true == "1"; // true
        echo false == ""; // true
    ?>
</body>
</html>