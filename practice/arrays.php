<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
        // *************** Arrays ***************
        
        $peopleOne = ['Diego', 'Ben', 'Ann'];
        echo $peopleOne[1]; 

        $peopleTwo = array('ken', 'chun-li');
        echo $peopleTwo[1]; 

        print_r(array_merge($peopleOne, $peopleTwo));

        $ages = [20, 30, 40, 50];
        print_r($ages); // not echo $ages;, echo needs string 

        $ages[1] = 25; // overwrites/replaces the value
        //print_r($ages);

        $ages[] = 60; // adds it to the end of  array
        //print_r($ages);

        array_push($ages, 70); // adds it to the end of  array
        //print_r($ages);

        echo count($ages); // counts the number of items in array

        // *************** Associative Arrays (key + value pairs) ***************

        $ninjasOne = ['Shaun' => 'black', 'ben' => 'orange'];
        $ninjasTwo = array('bowser' => 'green', 'peach' => 'pink');

        echo $ninjasOne['Shaun']; // black

        $ninjasTwo['Toad'] = 'yellow'; // adds key Toad and value yello to array
        print_r($ninjasTwo);

        echo count($ninjasTwo); // gives 3 

        // *************** Multidimensional Arrays (Arrays within arrays) ***************

        /*
        $games = [
            ['Mario Party', 'multiplayer', 'lorem', 30],
            ['Legend of Zelda', 'adventure', 'lorem', 50],
            ['Metroid', 'thriller', 'lorem', 40]
        ];

        print_r($games[1][1]);

        */

        $games = [
            ['title' => 'Mario Party', 'genre' => 'multiplayer', 'content' => 'lorem', 'hours' => 30],
            ['title' => 'Legend of Zelda', 'genre' => 'adventure', 'content' => 'lorem', 'hours' => 50],
            ['title' => 'Metroid', 'genre' => 'thriller', 'content' => 'lorem', 'hours' => 40]
        ];

        echo $games[2]['genre'];
        
        array_pop($games); // removes the last item of array
    ?>
</body>
</html>