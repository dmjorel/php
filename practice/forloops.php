<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        // *************** For Loops ***************

        $peopleOne = ['Diego', 'Ben', 'Ann'];
        for ($i = 0; $i < count($peopleOne); $i++){
            echo $peopleOne[$i] . '<br/>';
        }

        foreach($peopleOne as $people){
            echo $people . '<br/>';
        }

        $games = [
            ['title' => 'Mario Party', 'genre' => 'multiplayer', 'content' => 'lorem', 'hours' => 30],
            ['title' => 'Legend of Zelda', 'genre' => 'adventure', 'content' => 'lorem', 'hours' => 50],
            ['title' => 'Metroid', 'genre' => 'thriller', 'content' => 'lorem', 'hours' => 40]
        ];
        
        foreach($games as $game){
            echo $game['title'] . ' - ' . $game['genre'];
            echo '<br/>';
        }
        
        $j = 0;
        while($j < count($games)){
            echo $games[$j]['hours'];
            echo '<br/>';
            $j++;
        }

        

    ?>
    <ul>
        <?php foreach($games as $game){ ?>
        
            <h3> <?php echo $game['title'];?> </h3>
            <p>  <?php echo $game['hours']?> hours </p>

        <?php } ?>
    </ul>
</body>
</html>