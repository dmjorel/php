<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        // *************** Functions ***************

        function sayHello(){
            echo "good morning yoshi";
        }

        // need to call/invoke function
        sayHello();
        echo '<br/>';

        function sayHey($name){
            echo "good morning $name";
        }

        sayHey('mario');
        echo '<br/>';

        function sayYo($name, $time){
            echo "good $time, $name";
        }

        sayYo('mario' 'morning');
        echo '<br/>';


        function formatGame($game){
            //echo "{$game['title']} has {$game['hours']} hours <br/>";
            return "{$game['title']} has {$game['hours']} hours <br/>";
        }

        $formatted = formatGame(['title'=>'Mario Kart', 'hours'=>40]);
        echo $formatted;

        $name = 'mario';

        function sayIt(){
            global $name; // making a variable global 
            // $name = 'wario'; only changes variable locally
            echo $name; // local variable
        }

        sayIt();

    ?>
</body>
</html>