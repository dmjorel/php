<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        // *************** Strings ***************

        $stringOne = 'my email is ';
        $stringTwo = 'diego@gmail.com';

        echo $stringOne . $stringTwo; // Concatination
        // echo 'my email is ' . $stringTwo; Also works
        echo "<br>"; // next line

        echo "the ninja screamed \"waaaaaaaaaaaaaah\"";
        echo "<br>"; // next line
        echo 'the ninja screamed "waaaaaaaaaaaaaah"'; // also works
        echo "<br>"; // next line

        echo $stringTwo[0]; // d
        echo $stringTwo[1]; // i
        echo $stringTwo[2]; // e
        echo $stringTwo[3]; // g
        echo $stringTwo[4]; // o

        echo "<br>"; // next line
        echo strlen($newName); // length of string = 5

        echo "<br>"; // next line
        echo strtoupper($newName); // Capitalizes all letters

        echo "<br>"; // next line
        echo str_replace('D', 'M', $newName); // what is to be replaced, what to replace with, from where

        echo "<br>"; // next line
    ?>
</body>
</html>