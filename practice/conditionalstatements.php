<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        // *************** Conditional statements ***************

        $price = 30;

        if($price < 30){
            echo 'the condition is met';
        } elseif($price > 30){
            echo 'the condition is wonky';    
        } else {
            echo 'the conditon is not met';
        }

        $games = [
            ['title' => 'Mario Party', 'genre' => 'multiplayer', 'content' => 'lorem', 'hours' => 30],
            ['title' => 'Legend of Zelda', 'genre' => 'adventure', 'content' => 'lorem', 'hours' => 50],
            ['title' => 'Metroid', 'genre' => 'thriller', 'content' => 'lorem', 'hours' => 40]
        ];

        // in a foreach, break stops the loop
        // in a foreach, continues skips the item from the loop

    ?>
    <div>
        <ul>
            <?php foreach($games as $game){ ?>
                <?php if($game['hours'] > 35){ ?>
                    <li><?php echo $game['title'];?></li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
</body>
</html>