<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        // *************** Variables ***************

        $newName = "Diego";
        echo "My name is $newName";
        // echo 'My name is $newName'; incorrect
        //$newName = "Marco"; can be overwritten
        //define('NAME', 'Diego'); Constant, cannot be overwritten

        echo "<br>"; // next line

        $newAge = 34;
        echo $newAge;

        echo "<br>"; // next line
    ?>
</body>
</html>